'use strict';

var through = require("through2");
var gutil = require("gulp-util");
var path = require("path");
var Buffer = require("buffer").Buffer;
var ConcatWithSourceMaps = require("concat-with-sourcemaps");

var helpers = {
normal: function normal(base, refer) {
	var dot = refer.indexOf(".");
	if (dot >= 0) {
		if (dot === 0) {
			refer = base + "/" + refer;
		}
		var parts = refer.replace(/\/([.]\/)+/g, "/").split("/");
		for (var i = 1; i < parts.length; i++) {
			if (i && parts[i] === ".." && parts[i - 1] !== "..") {
				parts.splice(i - 1, 2);
				i -= 2;
			}
		}
		refer = parts.join("/");
	}
	return refer;
},
define: function def(id, deps, cb) {
	if (cb === undefined) {
		cb = deps;
		deps = [ ];
	}
	for (var i = 0, fn; i < deps.length; i++) {
		(fn = cache[normal(id, deps[i])]) && !fn.__defined__ && fn();
	}
	if (!cache[id].__defined__) {
		define.apply(null, arguments);
		cache[id].__defined__ = true;
	}
},
run: function run() {
	if (window.dojo) {
		cache["*noref"] = true;
		require({ cache: cache });
	} else {
		for (var id in cache) {
			cache[id]();
		}
	}
}
};

function normalizeId(moduleId, baseModuleId) {
	var idxOfDot = moduleId.indexOf(".");
	if (idxOfDot >= 0) {
		if (idxOfDot === 0) {
			moduleId = baseModuleId + moduleId;
		}
		var parts = moduleId.replace(/\/(\.\/)+/g, "/").split("/");
		for (var i = 1; i < parts.length; i++) {
			if (i && parts[i] === ".." && parts[i - 1] !== "..") {
				parts.splice(i - 1, 2);
				i -= 2;
			}
		}
		moduleId = parts.join("/");
	}
	return moduleId;
}

function getDefine(options) {
	return function (path, id, deps, def) {
		if (arguments.length === 2) { // assume define(path, def)
			def = id;
			deps = [ ];
			id = null;
		} else if (arguments.length === 3) {
			if (typeof id === "string") { // assume define(path, id, def)
				def = deps;
				deps = [ ];
			} else { // assume define(path, deps, def)
				def = deps;
				deps = id || [ ];
				id = null;
			}
		}

		var mid = (options.moduleRoot + path).slice(0, -3);

		var base = mid.substr(0, mid.lastIndexOf("/") + 1);
		for (var i = 0; i < deps.length; i++) {
			deps[i] = normalizeId(deps[i], base);
		}

		return {
			id: mid,
			deps: deps,
			loaded: false
		};
	};
}

function sort(options) {
	options = options || { };
	options.moduleRoot = options.moduleRoot || "";
	options.debug = !!options.debug;
	options.moduleIds = !!options.moduleIds;

	if (/[^/]$/.test(options.moduleRoot)) {
		options.moduleRoot += "/";
	}

	var define = getDefine(options);

	var offset = 0;
	var amdFiles = [ ];
	var otherFiles = [ ];
	var modules = { };

	return through.obj(
		function(file, encoding, callback) {
			if (file.isNull()) {
				callback();
				return;
			}
			var module, content, parts;
			if (file.relative.toLowerCase().substr(-3) === ".js") {
				content = file.contents.toString();
				parts = content.split(/(^|\s)define\s*\(/);
				if (parts.length > 1) {
					content = parts[0] + parts[1] + 'define("' + file.relative.replace(/\\/g, "/") + '",' + parts[2];
					module = eval(content);
					module.file = file;
					if (options.moduleIds) {
						module.file.contents = new Buffer(content.replace(/(^|\s)define\s*\(\s*(["'].+?["'],)?/, '$1define("' + module.id + '",'));
					}
					modules[module.id] = module;
				} else {
					otherFiles.push(file);
				}
			} else {
				otherFiles.push(file);
			}
			callback();
		},
		function(callback) {
			var mid, i, depsOk, allLoaded = false, someLoaded = false;

			if (options.debug) {
				console.log("Dependencies: ");
				for (mid in modules) {
					if (options.debug) {
						console.log("  " + mid);
					}
					for (i = 0; i < modules[mid].deps.length; i++) {
						if (options.debug) {
							console.log("    " + modules[mid].deps[i] + (modules[modules[mid].deps[i]] ? "" : " (ignored)"));
						}
					}
				}
			}
			while (!allLoaded) {
				allLoaded = true;
				someLoaded = false;
				for (mid in modules) {
					if (!modules[mid].loaded) {
						allLoaded = false;
						depsOk = true;
						for (i = 0; depsOk && i < modules[mid].deps.length; i++) {
							depsOk = !modules[modules[mid].deps[i]] || modules[modules[mid].deps[i]].loaded;
						}
						if (depsOk) {
							someLoaded = true;
							modules[mid].loaded = true;
							amdFiles.push(modules[mid].file);
						}
					}
				}
				if (!(allLoaded || someLoaded)) {
					throw new Error("gulp-amd-util cannot handle circular dependencies yet!");
				}
			}

			otherFiles.forEach(function(file) { this.push(file); }, this);
			amdFiles.forEach(function(file) { this.push(file); }, this);

			if (options.debug) {
				console.log("File order: ");
				otherFiles.forEach(function(file) { console.log("  " + file.relative); });
				amdFiles.forEach(function(file) { console.log("  " + file.relative); });
			}
			callback();
		}
	);
}

function bundle(options) {
	options = options || { };
	options.moduleRoot = options.moduleRoot || "";
	options.outputPath = options.outputPath || "bundle.js";
	options.keepSource = !!options.keepSource;
	options.debug = !!options.debug;

	if (/[^/]$/.test(options.moduleRoot)) {
		options.moduleRoot += "/";
	}

	var define = getDefine(options);

	var concat = new ConcatWithSourceMaps(true, options.outputPath, gutil.linefeed);
	var hasSourceMap = false;
	var outputFiles = [ ];
	var bundleFiles = [ ];
	var bundleFile = null;

	concat.add(null, "/* generated with https://www.npmjs.com/package/gulp-amd-util */");
	concat.add(null, "(function(){");
	concat.add(null, "var cache = {");

	return through.obj(
		function(file, encoding, callback) {
			if (file.isNull()) {
				callback();
				return;
			}
			var content, mid;
			if (file.relative.toLowerCase().substr(-3) === ".js") {
				mid = (options.moduleRoot + file.relative.replace(/\\/g, "/")).slice(0, -3);
				bundleFile = bundleFile || new gutil.File({
					cwd: file.cwd,
					base: file.base,
					path: path.join(file.base, options.outputPath)
				});
				hasSourceMap = hasSourceMap || !!file.sourceMap;
				content = file.contents.toString();
				content = new Buffer(content.replace(/(^|\s)define\s*\(\s*(["'].+?["'],)?/, '$1def("' + mid + '",'));
				content = '"' + mid + '": function() {\n' + content + '\n},';
				concat.add(file.relative, content, file.sourceMap);
			}
			if (options.keepSource) {
				outputFiles.push(file);
			}
			if (options.debug) {
				bundleFiles.push(file);
			}
			callback();
		},
		function(callback) {
			if (bundleFile) {
				concat.add(null, "};");
				for (var i in helpers) {
					concat.add(null, helpers[i].toString());
				}
				concat.add(null, "run();");
				concat.add(null, "})();");
				concat.add(null, 'define("' + define(bundleFile.relative.replace(/\\/g, "/"), 1).id + '", [], 1);');
				bundleFile.contents = concat.content;
				if (hasSourceMap) {
					bundleFile.sourceMap = JSON.parse(concat.sourceMap);
				}
				if (options.debug) {
					console.log("Bundle includes " + bundleFiles.length + " files:");
					bundleFiles.forEach(function(file) { console.log("  " + file.relative); });
				}
				outputFiles.push(bundleFile);
			} else if (options.debug) {
				console.log("No files to bundle");
			}
			outputFiles.forEach(function(file) { this.push(file); }, this);
			callback();
		}
	);
}

module.exports = function() {
	throw "No default function in gulp-amd-util. Call either sort or bundle instead";
};
module.exports.sort = sort;
module.exports.bundle = bundle;
