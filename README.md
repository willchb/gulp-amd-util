gulp-amd-util
==============

This provides couple utilities for working with Javascript AMD modules and gulp pipes.

With this you can create javascript bundles for AMD modules, if you're using [RequireJS](http://requirejs.org), or [Dojo](https://dojotoolkit.org), or [Babel](https://babeljs.io) with [transform-es2015-modules-amd](https://www.npmjs.com/package/babel-plugin-transform-es2015-modules-amd) plugin, among others.


Quick start
------------

### Installing with npm

``` shell
npm install gulp-amd-util --save-dev
```

### Javascript bundle task example *(up to version 0.2.0.rc.0)*
``` js
var gulp = require("gulp");
var gulp_amd_util = require("gulp-amd-util");

// up to version 0.2.0.rc.0, need to call sort before bundle
// and also moduleRoot must have a trailing slash.
gulp.task("js-bundle", function() {
	return gulp.src("source/**/*.js").pipe(
		gulp_amd_util.sort({
			moduleRoot: "my-app/"
		})
	).pipe(
		gulp_amd_util.bundle({
			moduleRoot: "my-app/",
			keepSource: true,
			outputPath: "my-app.bundle.js"
		})
	).pipe(gulp.dest("dist/");
});
```

### Javascript bundle task example *(version 0.2.1 and up)*
``` js
var gulp = require("gulp");
var gulp_amd_util = require("gulp-amd-util");

// version 0.2.1 and up, no need to call sort before bundle
gulp.task("js-bundle", function() {
	return gulp.src("source/**/*.js").pipe(
		gulp_amd_util.bundle({
			moduleRoot: "my-app",
			keepSource: true,
			outputPath: "my-app.bundle.js"
		})
	).pipe(gulp.dest("dist/");
});
```

### Using babel to transpile ECMAScript2015 code to AMD first
```js
import gulp from "gulp";
import gulp_amd_util from 'gulp-amd-util';
import gulp_babel from "gulp-babel";
import gulp_sourcemaps from "gulp-sourcemaps";

const BABEL_CONFIG = {
    "plugins": [
        "external-helpers",
        "add-module-exports",
        "transform-es2015-modules-amd"
    ],
    "presets": [ "es2015" ],
    "babelrc": false,
    "moduleIds": false
};

gulp.task("js", () => gulp.src("source/**/*.js")
    .pipe(gulp_sourcemaps.init())
    .pipe(gulp_babel(BABEL_CONFIG))
    .pipe(gulp_amd_util.bundle({
        moduleRoot: "my-app",
        keepSource: true,
        outputPath: "my-app.bundle.js"
    }))
    .pipe(gulp_sourcemaps.write("."))
    .pipe(gulp.dest("dist"))
);
```

### Options

#### sort *(obsolete if calling bundle since 0.2.1)*:

- **moduleRoot**: defines the base module id. Assuming a `moduleRoot: "my-company/my-app"`, then a module defined in `form/input.js` will become `"my-company/my-app/form/input"` after the transformation.
- **moduleIds**: if it's `true`, the module ids are added/fixed not only in the bundle, but in the individual files as well.
- **debug**: produces more verbose logs

#### bundle:

- **moduleRoot**: defines the base module id. Assuming a `moduleRoot: "my-company/my-app"`, then a module defined in `form/input.js` will become `"my-company/my-app/form/input"` after the transformation.
- **outputPath**: the file to write the bundle to.
- **keepSource**: if `true`, the original files included in the bundle are kept after the transformation.
- **style** *(obsolete since 0.2.1)*: style of the bundle. The accepted values are: `"dojo"`, `"requirejs"`.
- **debug**: produces more verbose logs
